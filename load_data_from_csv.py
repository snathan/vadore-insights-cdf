# -*- coding: utf-8 -*-

import platform

import pandas as pd
import numpy as np

if platform.system() == 'Linux':
    MAIN_PATH = "/data/rctnet/"
else:
    MAIN_PATH = "//Srvsto/rctnet/"


DATASET_PATH= MAIN_PATH + "FAIRNESS/newsample/"

#Permet de charger iterativement un csv tout en le filtrant à la volée par chunk de 100 000 lignes
#Attend une liste de semaines ex : weeks = ['2021-42','2021-43','2021-44']
def iterative_load_and_filter(stream, var = None, values = None):
    gen = pd.read_csv(stream, chunksize=100000, iterator=True)
    df = pd.concat([chunk[chunk[var].isin(values)] if values is not None and var in chunk.columns else chunk for chunk in gen])
    return df

def filter_csv(df, var=None, values=None):
    # filter the data to keep (for train/test split for example)
    if var is not None and values is not None:
        df = df[ df[var].isin(values) ]

    # remove the index if it exist as a column
    if "Unnamed: 0" in df.columns:
        del df["Unnamed: 0"]

    return df



# time = 4s
# shape = (6 893 731, 2)
# key = ['semaine','kc_offre']
def load_available_offers(var = None, values = None):
    df_available_offers = pd.read_csv(DATASET_PATH + 'available_offers.csv')
    df_available_offers = filter_csv(df_available_offers, var=var, values=values)
    return df_available_offers

def load_available_offers_beta(var = None, values = None):
    df_available_offers = pd.read_csv(DATASET_PATH + 'available_offers_beta.csv')
    df_available_offers = filter_csv(df_available_offers, var=var, values=values)
    return df_available_offers

# time = 50s
# shape = (1 495 140, 83)
# key = 'kc_offre'
def load_offers(var = None, values = None):

    df_offers = pd.read_csv(DATASET_PATH + 'offers.csv')
    df_offers = filter_csv(df_offers, var=var, values=values)
    df_offers['niv_formation'] = df_offers['niv_formation'].astype(str)
    df_offers['domaine_formation'] = df_offers['domaine_formation'].astype(str)

    return df_offers

def load_offers_beta(var = None, values = None):

    df_offers = pd.read_csv(DATASET_PATH + 'offers_beta.csv')
    df_offers = filter_csv(df_offers, var=var, values=values)
    df_offers['niv_formation'] = df_offers['niv_formation'].astype(str)
    df_offers['domaine_formation'] = df_offers['domaine_formation'].astype(str)

    return df_offers

def load_etabs():
    etabs = pd.read_csv(DATASET_PATH + 'offers.csv', usecols=['kc_offre', 'kn_etablissement'])
    return etabs

def load_etabs_beta():
    etabs = pd.read_csv(DATASET_PATH + 'offers_beta.csv', usecols=['kc_offre', 'kn_etablissement'])
    return etabs


# time = 31s
# shape = (6 893 731, 24)
# key = ['semaine','kc_offre']
def load_offer_time_features(var = None, values = None):
    df_offer_time_features = pd.read_csv(DATASET_PATH + 'offer_time_features.csv')
    df_offer_time_features = filter_csv(df_offer_time_features, var=var, values=values)
    return df_offer_time_features

def load_offer_time_features_beta(var = None, values = None):
    df_offer_time_features = pd.read_csv(DATASET_PATH + 'offer_time_features_beta.csv')
    df_offer_time_features = filter_csv(df_offer_time_features, var=var, values=values)
    return df_offer_time_features

# time = 50s
# shape = (67 421 380, 2)
# key = ['semaine','session_id']
def load_available_jobseekers(var = None, values = None):
    df_available_jobseekers = pd.read_csv(DATASET_PATH + 'available_jobseekers.csv')
    df_available_jobseekers = filter_csv(df_available_jobseekers, var=var, values=values)
    return df_available_jobseekers

def load_available_jobseekers_beta(var = None, values = None):
    df_available_jobseekers = pd.read_csv(DATASET_PATH + 'available_jobseekers_beta.csv')
    df_available_jobseekers = filter_csv(df_available_jobseekers, var=var, values=values)
    return df_available_jobseekers

# time = 52s
# shape = (2 142 977, 91)
# key = 'session_id'
def load_jobseekers(var = None, values = None):
    df_jobseekers = pd.read_csv(DATASET_PATH + 'jobseekers.csv')
    df_jobseekers = filter_csv(df_jobseekers, var=var, values=values)
    df_jobseekers[['motins']] = df_jobseekers[['motins']].fillna(99).astype(int)
    df_jobseekers['sitpar'] = df_jobseekers['sitpar'].astype(str)
    df_jobseekers[['catregr']] = df_jobseekers[['catregr']].fillna(0).astype(int)
    df_jobseekers['rome'] = df_jobseekers['rome'].astype(str)
    df_jobseekers['salunit'] = df_jobseekers['salunit'].astype(str)
    df_jobseekers['qualif'] = df_jobseekers['qualif'].fillna(0).astype(int).astype(str).replace('0', np.nan).astype(str)
    df_jobseekers['depcom'] = df_jobseekers['depcom'].astype(str)
    df_jobseekers['departement'] = df_jobseekers['departement'].astype(str)
    df_jobseekers['secteur_1'] = df_jobseekers['secteur_1'].astype(str)
    df_jobseekers['secteur_2'] = df_jobseekers['secteur_2'].astype(str)
    df_jobseekers['niv_formation_max'] = df_jobseekers['niv_formation_max'].astype(str)
    df_jobseekers['domaine_formation'] = df_jobseekers['domaine_formation'].astype(str)

    return df_jobseekers

def load_jobseekers_beta(var = None, values = None):
    df_jobseekers = pd.read_csv(DATASET_PATH + 'jobseekers_beta.csv')
    df_jobseekers = filter_csv(df_jobseekers, var=var, values=values)
    df_jobseekers[['motins']] = df_jobseekers[['motins']].fillna(99).astype(int)
    df_jobseekers['sitpar'] = df_jobseekers['sitpar'].astype(str)
    df_jobseekers[['catregr']] = df_jobseekers[['catregr']].fillna(0).astype(int)
    df_jobseekers['rome'] = df_jobseekers['rome'].astype(str)
    df_jobseekers['salunit'] = df_jobseekers['salunit'].astype(str)
    df_jobseekers['qualif'] = df_jobseekers['qualif'].fillna(0).astype(int).astype(str).replace('0', np.nan).astype(str)
    df_jobseekers['depcom'] = df_jobseekers['depcom'].astype(str)
    df_jobseekers['departement'] = df_jobseekers['departement'].astype(str)
    df_jobseekers['secteur_1'] = df_jobseekers['secteur_1'].astype(str)
    df_jobseekers['secteur_2'] = df_jobseekers['secteur_2'].astype(str)
    df_jobseekers['niv_formation_max'] = df_jobseekers['niv_formation_max'].astype(str)
    df_jobseekers['domaine_formation'] = df_jobseekers['domaine_formation'].astype(str)

    return df_jobseekers

# time = 282s (5min)
# shape = (67 421 380, 17)
# key = ['semaine','session_id']
def load_jobseeker_time_features(var = None, values = None):

    cols1 = pd.read_csv(DATASET_PATH + 'jobseeker_time_features_select_col_1.csv')
    cols1 = filter_csv(cols1, var=var, values=values)

    cols2 = pd.read_csv(DATASET_PATH + 'jobseeker_time_features_select_col_2.csv')
    cols2 = filter_csv(cols2, var=var, values=values)

    all_data = cols1.merge(cols2, on=['semaine', 'session_id'], how='left')
    del cols1, cols2

    cols3 = pd.read_csv(DATASET_PATH + 'jobseeker_time_features_select_col_3.csv')
    cols3 = filter_csv(cols3, var=var, values=values)

    all_data = all_data.merge(cols3, on=['semaine', 'session_id'], how='left')

    return all_data

def load_jobseeker_time_features_in_matches(var = None, values = None):
    time_features = pd.read_csv(DATASET_PATH + 'jobseeker_time_features_in_matches.csv')
    time_features = filter_csv(time_features, var=var, values=values)
    return time_features

def load_jobseeker_time_features_beta(var = None, values = None):

    cols1 = pd.read_csv(DATASET_PATH + 'jobseeker_time_features_select_col_1_beta.csv')
    cols1 = filter_csv(cols1, var=var, values=values)

    cols2 = pd.read_csv(DATASET_PATH + 'jobseeker_time_features_select_col_2_beta.csv')
    cols2 = filter_csv(cols2, var=var, values=values)

    all_data = cols1.merge(cols2, on=['semaine', 'session_id'], how='left')
    del cols1, cols2

    cols3 = pd.read_csv(DATASET_PATH + 'jobseeker_time_features_select_col_3_beta.csv')
    cols3 = filter_csv(cols3, var=var, values=values)

    all_data = all_data.merge(cols3, on=['semaine', 'session_id'], how='left')

    return all_data


# time = 36s
# shape = (82 338 748, 2)
# key = ['session_id','savoir_faire']
def load_savoir_faire_demandeur_etendus(var = None, values = None):
    df_savoir_faire_demandeur_etendus = pd.read_csv(DATASET_PATH + 'savoir_faire_demandeur_etendus.csv')
    df_savoir_faire_demandeur_etendus = filter_csv(df_savoir_faire_demandeur_etendus, var=var, values=values)
    return df_savoir_faire_demandeur_etendus

def load_savoir_faire_demandeur_etendus_beta(var = None, values = None):
    df_savoir_faire_demandeur_etendus = pd.read_csv(DATASET_PATH + 'savoir_faire_demandeur_etendus_beta.csv')
    df_savoir_faire_demandeur_etendus = filter_csv(df_savoir_faire_demandeur_etendus, var=var, values=values)
    return df_savoir_faire_demandeur_etendus

def load_savoir_faire_demandeur_appellation(var = None, values = None):
    sf_appellation = pd.read_csv(DATASET_PATH + 'savoir_faire_demandeur_appellation.csv')
    sf_appellation = filter_csv(sf_appellation, var=var, values=values)
    return sf_appellation

# time = 12s
# shape = (33 010 871, 2)
# key = ['kc_offre','savoir_faire']
def load_savoir_faire_offres_etendus(var = None, values = None):
    df_savoir_faire_offres_etendus = pd.read_csv(DATASET_PATH + 'savoir_faire_offres_etendus.csv')
    df_savoir_faire_offres_etendus = filter_csv(df_savoir_faire_offres_etendus, var=var, values=values)
    return df_savoir_faire_offres_etendus

def load_savoir_faire_offres_etendus_beta(var = None, values = None):
    df_savoir_faire_offres_etendus = pd.read_csv(DATASET_PATH + 'savoir_faire_offres_etendus_beta.csv')
    df_savoir_faire_offres_etendus = filter_csv(df_savoir_faire_offres_etendus, var=var, values=values)
    return df_savoir_faire_offres_etendus

def load_savoir_faire_offres_appellation(var = None, values = None):
    sf_appellation = pd.read_csv(DATASET_PATH + 'savoir_faire_offres_appellation.csv')
    sf_appellation = filter_csv(sf_appellation, var=var, values=values)
    return sf_appellation

# time = 1s
# shape = (225 370, 5)
# key = ['session_id','kc_offre']
def load_matches(var = None, values = None):
    df_matches = pd.read_csv(DATASET_PATH + 'matches.csv')
    df_matches = filter_csv(df_matches, var=var, values=values)
    return df_matches


# time = 9s
# shape = (37 144, 574)
# key = 'commune_insee'
def load_geo_grid():
    df_geo_grid = pd.read_csv(DATASET_PATH + 'geo_grid.csv')
    df_geo_grid['commune_insee'] = [i.lstrip('0') for i in list(df_geo_grid['commune_insee'].astype(str))]
    return df_geo_grid


# time = 2s
# shape = (2 142 977, 2)
# key = 'session_id'
def load_base_jobseeker_data(var = None, values = None):
    df_base_jobseeker_data = pd.read_csv(DATASET_PATH + 'base_jobseeker_data.csv')
    df_base_jobseeker_data = filter_csv(df_base_jobseeker_data, var=var, values=values)
    df_base_jobseeker_data['depcom'] = [i.lstrip('0') for i in list(df_base_jobseeker_data['depcom'].astype(str))]
    df_base_jobseeker_data['depcom'] = df_base_jobseeker_data['depcom'].astype(str)
    return df_base_jobseeker_data

def load_base_jobseeker_data_beta(var = None, values = None):
    df_base_jobseeker_data = pd.read_csv(DATASET_PATH + 'base_jobseeker_data_beta.csv')
    df_base_jobseeker_data = filter_csv(df_base_jobseeker_data, var=var, values=values)
    df_base_jobseeker_data['depcom'] = [i.lstrip('0') for i in list(df_base_jobseeker_data['depcom'].astype(str))]
    df_base_jobseeker_data['depcom'] = df_base_jobseeker_data['depcom'].astype(str)
    return df_base_jobseeker_data

# time = 1s
# shape = (1 495 140, 2)
# key = 'kc_offre'
def load_raw_offer_data(var = None, values = None):
    df_raw_offer_data = pd.read_csv(DATASET_PATH + 'raw_offer_data.csv')
    df_raw_offer_data = filter_csv(df_raw_offer_data, var=var, values=values)
    df_raw_offer_data['dc_communelieutravail'] = df_raw_offer_data['dc_communelieutravail'].astype(str)
    return df_raw_offer_data

def load_raw_offer_data_beta(var = None, values = None):
    df_raw_offer_data = pd.read_csv(DATASET_PATH + 'raw_offer_data_beta.csv')
    df_raw_offer_data = filter_csv(df_raw_offer_data, var=var, values=values)
    df_raw_offer_data['dc_communelieutravail'] = df_raw_offer_data['dc_communelieutravail'].astype(str)
    return df_raw_offer_data

# time = 6s
# shape = (2 142 977, 14)
# key = 'session_id'
def load_criteres_demandeurs_modele_pref(var = None, values = None):
    df_criteres_demandeurs_modele_pref = pd.read_csv(DATASET_PATH + 'criteres_demandeurs_modele_pref.csv')
    df_criteres_demandeurs_modele_pref = filter_csv(df_criteres_demandeurs_modele_pref, var=var, values=values)
    return df_criteres_demandeurs_modele_pref


# time = 4s
# shape = (1 495 140, 16)
# key = 'kc_offre'
def load_criteres_offres_modele_pref(var = None, values = None):
    df_criteres_offres_modele_pref = pd.read_csv(DATASET_PATH + 'criteres_offres_modele_pref.csv')
    df_criteres_offres_modele_pref = filter_csv(df_criteres_offres_modele_pref, var=var, values=values)
    return df_criteres_offres_modele_pref


# time = 1s
# shape = (417 155, 4)
# key = ['session_id','kc_offre','semaine']
def load_clicks_test(var = None, values = None):
    df_clicks_test = pd.read_csv(DATASET_PATH + 'clicks_test.csv')
    df_clicks_test = filter_csv(df_clicks_test, var=var, values=values)
    return df_clicks_test


# time = 11s
# shape = (1 704 527, 4)
# key = ['session_id','kc_offre','semaine']
def load_clicks_train(var = None, values = None):
    df_clicks_train = pd.read_csv(DATASET_PATH + 'clicks_train.csv')
    df_clicks_train = filter_csv(df_clicks_train, var=var, values=values)
    return df_clicks_train


# time = 3s
# shape = (1 495 140, 11)
# key = 'kc_offre'
def load_criteres_offres(var = None, values = None):
    df_criteres_offres = pd.read_csv(DATASET_PATH + 'criteres_offres.csv')
    df_criteres_offres = filter_csv(df_criteres_offres, var=var, values=values)
    return df_criteres_offres



def load_jobseekers_vad_all():
    df_jobseekers = pd.read_csv(DATASET_PATH + 'jobseekers_in_vadore_all.csv')
    df_jobseekers[['motins']] = df_jobseekers[['motins']].fillna(99).astype(int)
    df_jobseekers['sitpar'] = df_jobseekers['sitpar'].astype(str)
    df_jobseekers[['catregr']] = df_jobseekers[['catregr']].fillna(0).astype(int)
    df_jobseekers['rome'] = df_jobseekers['rome'].astype(str)
    df_jobseekers['salunit'] = df_jobseekers['salunit'].astype(str)
    df_jobseekers['qualif'] = df_jobseekers['qualif'].fillna(0).astype(int).astype(str).replace('0', np.nan).astype(str)
    df_jobseekers['depcom'] = df_jobseekers['depcom'].astype(str)
    df_jobseekers['departement'] = df_jobseekers['departement'].astype(str)
    df_jobseekers['secteur_1'] = df_jobseekers['secteur_1'].astype(str)
    df_jobseekers['secteur_2'] = df_jobseekers['secteur_2'].astype(str)
    df_jobseekers['niv_formation_max'] = df_jobseekers['niv_formation_max'].astype(str)
    df_jobseekers['domaine_formation'] = df_jobseekers['secteur_2'].astype(str)

    return df_jobseekers


def load_jobseeker_skills_vad_all():
    df_savoir_faire_demandeur_etendus = pd.read_csv(DATASET_PATH + 'jobseeker_skills_in_vad_all.csv')
    return df_savoir_faire_demandeur_etendus

def load_matches_vad_all():
    matches_vad_all = pd.read_csv(DATASET_PATH + 'matches_vadore_all.csv')
    return matches_vad_all

def load_offers_train_vad_all():
    offers_train_vad_all = pd.read_csv(DATASET_PATH + 'offers_train_vad_all.csv')
    offers_train_vad_all['niv_formation'] = offers_train_vad_all['niv_formation'].astype(str)
    return offers_train_vad_all

def load_offers_skills_train_vad_all():
    offers_skills_train_vad_all = pd.read_csv(DATASET_PATH + 'offers_skills_train_vad_all.csv')
    return offers_skills_train_vad_all

def load_treatment_groups():
    treatment_groups = pd.read_csv(DATASET_PATH + 'groupes_traitement.csv')
    return treatment_groups

def load_crit_offers():
    crit = pd.read_csv(DATASET_PATH + 'profil_offres_modele_criteres.csv')
    return crit

def load_crit_offers_beta():
    crit = pd.read_csv(DATASET_PATH + 'profil_offres_modele_criteres_beta.csv')
    return crit

def load_crit_jobseekers():
    crit = pd.read_csv(DATASET_PATH + 'profil_demandeurs_modele_criteres.csv')
    return crit

def load_crit_jobseekers_beta():
    crit = pd.read_csv(DATASET_PATH + 'profil_demandeurs_modele_criteres_beta.csv')
    return crit
