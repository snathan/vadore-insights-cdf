import itertools

import numpy as np
import pandas as pd
from tqdm import tqdm

from load_data_from_csv import load_jobseekers, load_offers

emb_path = "/data/rctnet/FAIRNESS/fairness/fairness_newsample/models/vad0_embeddings/"
user_emb_path = emb_path + "jobseeker_embeddings.npy"
user_ids_path = emb_path + "jobseeker_ids.csv"
item_emb_path = emb_path + "offer_embeddings.npy"
item_ids_path = emb_path + "offer_ids.csv"

selected_rome = "D1504"
output_folder = "output"
compression_algo = "brotli"

print("loading embeddings")
user_emb = np.load(user_emb_path)
user_ids = pd.read_csv(user_ids_path, index_col=0)
item_emb = np.load(item_emb_path)
item_ids = pd.read_csv(item_ids_path, index_col=0)

print("loading jobseekers")
jobseekers = load_jobseekers()
print("loading offers")
offers = load_offers()

print("embeddings in col as list")
user_ids["embeddings"] = user_emb.tolist()
item_ids["embeddings"] = item_emb.tolist()

print("select rome for jobseekers")
selected_rome_ids = jobseekers[jobseekers["rome"] == selected_rome]["session_id"]
filtered_user_emb = user_ids[user_ids["session_id"].isin(selected_rome_ids)]

print("select ROME for offers")
selected_rome_ids = offers[offers["dc_rome_id"] == selected_rome]["kc_offre"]
filtered_item_emb = item_ids[item_ids["kc_offre"].isin(selected_rome_ids)]

print("saving intermediate results")
filtered_item_emb.to_parquet(
    f"{output_folder}/{selected_rome}_items_ids_emb.parquet.{compression_algo}",
    compression=f"{compression_algo}",
)
filtered_user_emb.to_parquet(
    f"{output_folder}/{selected_rome}_users_ids_emb.parquet.{compression_algo}",
    compression=f"{compression_algo}",
)

print("create a Cartesian product of the two dataframes")
cartesian_product = list(
    itertools.product(
        filtered_item_emb.itertuples(index=False),
        filtered_user_emb.itertuples(index=False),
    )
)

print("create a new dataframe with dot product")
result_df = pd.DataFrame(
    [
        (item.kc_offre, user.session_id, np.dot(item.embeddings, user.embeddings))
        for item, user in tqdm(cartesian_product)
    ],
    columns=["kc_offre", "session_id", "dot_product"],
)

print("save final result")
result_df.to_parquet(
    f"{output_folder}/{selected_rome}_sij_matrix.parquet.{compression_algo}",
    compression=f"{compression_algo}",
)
