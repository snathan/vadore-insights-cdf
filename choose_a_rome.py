import polars as pl

from load_data_from_csv import DATASET_PATH

min_user = 1000
max_user = 5000
min_item = 1000
max_item = 5000

q = (
    pl.scan_csv(DATASET_PATH + "jobseekers.csv")
    .select("rome")
    .group_by("rome")
    .agg(pl.len())
    .sort(by=pl.col("len"), descending=True)
    .filter(min_user < pl.col("len"))
    .filter(pl.col("len") < max_user)
    .rename({"len": "nb_user_in_rome"})
)


q2 = (
    pl.scan_csv(DATASET_PATH + "offers.csv")
    .select("dc_rome_id")
    .rename({"dc_rome_id": "rome"})
    .group_by("rome")
    .agg(pl.len())
    .sort(by=pl.col("len"), descending=True)
    .filter(min_item < pl.col("len"))
    .filter(pl.col("len") < max_item)
    .rename({"len": "nb_item_in_rome"})
)

jobseekers = q.collect()
offers = q2.collect()
result = jobseekers.join(offers, on="rome")

print(result)
